# Manadtory setting for minimal CMake requirement:
cmake_minimum_required( VERSION 2.8 )

# Create a project:
project( SUSY )

# Only necessary on MacOS X to silence a warning:
set( CMAKE_MACOSX_RPATH ON )

# Build the library:
add_library( NeutralinoDM SHARED lib/AnalysisInput.h lib/AnalysisInput.cxx lib/AnalysisBDT.h lib/AnalysisBDT.cxx lib/AnalysisCuts.h lib/AnalysisCuts.cxx)
target_include_directories( NeutralinoDM PUBLIC
   $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/lib>
   $<INSTALL_INTERFACE:include> )

# Find Boost:
find_package( Boost COMPONENTS program_options )

# Find ROOT:
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

# Check, if ROOTSYS is set.
if(NOT DEFINED ROOTSYS)
  set(ROOTSYS $ENV{ROOTSYS})
  if(NOT DEFINED ROOTSYS)
    message(FATAL_ERROR "ROOTSYS is not set!")
  endif()
endif()

# Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS):
#find_package(ROOT REQUIRED COMPONENTS RIO Net)
find_package(ROOT REQUIRED)
get_filename_component(ROOT_INCLUDE_DIR "${ROOTSYS}/include" ABSOLUTE)
set(ROOT_INCLUDE_DIRS ${ROOT_INCLUDE_DIR})
set(ROOT_LIBRARIES Core RIO Net Hist Gpad Tree Rint Matrix MathCore TMVA Physics)
set(ROOT_root_CMD ${ROOTSYS}/bin/root.exe)
set(ROOT_genreflex_CMD ${ROOTSYS}/bin/genreflex)
set(ROOT_rootcint_CMD ${ROOTSYS}/bin/rootcint)
set(ROOT_rootcling_CMD rootcling)

# include ROOT directories
message(${ROOT_INCLUDE_DIRS})
include_directories(${ROOT_INCLUDE_DIRS})

# include ROOT libraries
get_filename_component(ROOT_BINARY_DIR  "${ROOTSYS}/bin" ABSOLUTE)
get_filename_component(ROOT_LIBRARY_DIR "${ROOTSYS}/lib" ABSOLUTE)

# Detect bitness.
if(CMAKE_SYSTEM_PROCESSOR MATCHES amd64.*|x86_64.*|aarch64.*|ppc64.*
   OR (CMAKE_VERSION VERSION_LESS 3.0 AND CMAKE_SYSTEM_NAME STREQUAL Darwin) )
  set(64BIT 1)
  message("-- Check for bitness: Found 64 bit architecture.")
elseif(CMAKE_SYSTEM_PROCESSOR MATCHES i686.*|i386.*|x86.*)
  set(32BIT 1)
  message("-- Check for bitness: Found 32 bit architecture.")
endif()

# Find python.
if(ROOT_python_FOUND)
  find_package(PythonInterp)
  if(PYTHONINTERP_FOUND)
    execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "import sys;sys.stdout.write(sys.prefix)"
                    OUTPUT_VARIABLE PYTHON_PREFIX)
    set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} ${PYTHON_PREFIX})
  endif()
  find_package(PythonLibs)
endif()

# Find OpenGL 
find_library(OPENGL_gl_LIBRARY NAMES GL)

# Setup standard includes / links.
include_directories(${ROOT_INCLUDE_DIRS} ${ROOT_INCLUDE_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
link_directories(${ROOT_LIBRARY_DIR})

# Build the executable:
add_executable( RunInput app/RunInput.cxx )
target_link_libraries( RunInput NeutralinoDM )
target_include_directories( RunInput PRIVATE ${Boost_INCLUDE_DIRS} )
target_link_libraries( RunInput ${Boost_LIBRARIES} )
target_link_libraries( RunInput ${ROOT_LIBRARIES})

add_executable( RunCut app/RunCuts.cxx )
target_link_libraries( RunCut NeutralinoDM )
target_include_directories( RunCut PRIVATE ${Boost_INCLUDE_DIRS} )
target_link_libraries( RunCut ${Boost_LIBRARIES} )
target_link_libraries( RunCut ${ROOT_LIBRARIES})

add_executable( RunBDT app/RunBDT.cxx )
target_link_libraries( RunBDT NeutralinoDM )
target_include_directories( RunBDT PRIVATE ${Boost_INCLUDE_DIRS} )
target_link_libraries( RunBDT ${Boost_LIBRARIES} )
target_link_libraries( RunBDT ${ROOT_LIBRARIES})

# Install the components:
install( TARGETS NeutralinoDM RunInput RunCut RunBDT
   EXPORT Neutralino
   RUNTIME DESTINATION bin
   LIBRARY DESTINATION lib )  

# Install the header:
install( FILES lib/AnalysisInput.h
  DESTINATION include )

# Install a CMake description of this project:
install( EXPORT Neutralino
  DESTINATION cmake )

# Check warnings
if ( CMAKE_COMPILER_IS_GNUCC )
  set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wall")
endif ( CMAKE_COMPILER_IS_GNUCC )
if ( MSVC )
  set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} /W4")
endif ( MSVC )
