// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "AnalysisInput.h"

AnalysisInput::AnalysisInput(Option option)
{
  
  std::cout << std::endl;
  std::cout << "--- AnalysisInput :: Analysis on " << option;

  option_ = option;

  if ( option_ == sgn )  std::cout << ": Signal" <<std::endl;
  if ( option_ == ZZ )  std::cout << ": Background ZZ" <<std::endl;
  if ( option_ == WW )  std::cout << ": Background WW" <<std::endl;
  if ( option_ == WZ )  std::cout << ": Background WZ" <<std::endl;
  if ( option_ == ttZ )  std::cout << ": Background ttZ" <<std::endl;
  if ( option_ == ttW )  std::cout << ": Background ttW" <<std::endl;

}


AnalysisInput::~AnalysisInput()
{
  //delete tree_in;
  //delete tree_out;
  delete input;
  delete output;
  

}
 
void AnalysisInput::initialize()
{

  // Tree integer variables (no vectors) 
  ld_PT = 0;
  ld_ETA = 0;
  ld_PHI = 0;
  ld_charge = 0;
  ld_type = 0;
  
  sub_ld_PT = 0;
  sub_ld_ETA = 0;
  sub_ld_PHI = 0;
  sub_ld_charge = 0;
  sub_ld_type = 0;

  numberbjet = 0;
  numberlepton = 0;
  totalweight = 0;

  met_PT = 0;
  met_ETA = 0;
  met_PHI = 0;
}

void AnalysisInput::addBranches()
{

  if ( option_ == sgn )  outname = "Signal_Ntuple.root";
  if ( option_ == ZZ )  outname = "ZZ_Ntuple.root";
  if ( option_ == WW )  outname = "WW_Ntuple.root";
  if ( option_ == WZ )  outname = "WZ_Ntuple.root";
  if ( option_ == ttZ )  outname = "ttZ_Ntuple.root";
  if ( option_ == ttW )  outname = "ttW_Ntuple.root";
  
  output = new TFile( outname , "RECREATE");
  tree_out = new TTree("neutralino", "neutralino");

  tree_out->Branch("weight", &weight);
  tree_out->Branch("ld_PT", &ld_PT);
  tree_out->Branch("ld_ETA", &ld_ETA);
  tree_out->Branch("ld_PHI", &ld_PHI);
  tree_out->Branch("ld_charge", &ld_charge);
  tree_out->Branch("ld_type", &ld_type);  

  tree_out->Branch("sub_ld_PT", &sub_ld_PT);
  tree_out->Branch("sub_ld_ETA", &sub_ld_ETA);
  tree_out->Branch("sub_ld_PHI", &sub_ld_PHI);
  tree_out->Branch("sub_ld_charge", &sub_ld_charge);
  tree_out->Branch("sub_ld_type", &sub_ld_type);  

  tree_out->Branch("numberbjet", &numberbjet);
  tree_out->Branch("numberlepton", &numberlepton);

  tree_out->Branch("met_PT", &met_PT);
  tree_out->Branch("met_ETA", &met_ETA);
  tree_out->Branch("met_PHI", &met_PHI);
  tree_out->Branch("met_miss", &met_miss);

  tree_out->Branch("invarient_mass", &invarient_mass);

  tree_out->Branch("totalweight", &totalweight);

  


  if ( option_ == sgn )  fname = filesFolder + "Signal_Raw_Events.root";
  if ( option_ == ZZ )  fname = filesFolder + "ZZ_Events.root";
  if ( option_ == WW )  fname = filesFolder + "WW_Events.root";
  if ( option_ == WZ )  fname = filesFolder + "WZ_Events.root";
  if ( option_ == ttZ )  fname = filesFolder + "ttZ_Events.root";
  if ( option_ == ttW )  fname = filesFolder + "ttW_Events.root";

  
  if (!gSystem->AccessPathName( fname )) {
    input = TFile::Open( fname ); // check if file in local directory exists
  } 
  
  if (!input) {
    std::cout << "ERROR: could not open data file" << std::endl;
    exit(1);
  }
  
  std::cout << "--- Using input file: " << input->GetName() << std::endl;
  if ( option_ == sgn )  std::cout << "--- Select Signal sample" << std::endl;
  if ( option_ == ZZ )  std::cout << "--- Select Background ZZ sample" << std::endl;
  if ( option_ == WW )  std::cout << "--- Select Background WW sample" << std::endl;
  if ( option_ == WZ )  std::cout << "--- Select Background WZ sample" << std::endl;
  if ( option_ == ttZ )  std::cout << "--- Select Background ttZ sample" << std::endl;
  if ( option_ == ttW )  std::cout << "--- Select Background ttW sample" << std::endl;

  // Load TTreeReader with input tree file
  tree_in = (TTree*)input->Get("Delphes");
  fReader.SetTree(tree_in);

}

void AnalysisInput::finalize()
{
  
  // Write and close output file
  output->Write();
  output->Close();
  
  // finalize
  std::cout << "--- Finalize. " <<std::endl;
  
}

void AnalysisInput::execute()
{

  // Variables input-reader
  TTreeReaderArray<Float_t> r_muonpT(fReader, "Muon.PT");
  TTreeReaderArray<Float_t> r_muonETA(fReader, "Muon.Eta");
  TTreeReaderArray<Float_t> r_muonPHI(fReader, "Muon.Phi");
  TTreeReaderArray<int> r_muonCHARGE(fReader, "Muon.Charge");
  
  TTreeReaderArray<Float_t> r_electronpT(fReader, "Electron.PT");
  TTreeReaderArray<Float_t> r_electronETA(fReader, "Electron.Eta");
  TTreeReaderArray<Float_t> r_electronPHI(fReader, "Electron.Phi");
  TTreeReaderArray<int> r_electronCHARGE(fReader, "Electron.Charge");

  TTreeReaderArray<unsigned int> r_bjet(fReader, "Jet.BTag");
  
  TTreeReaderArray<Float_t> r_metmet(fReader, "MissingET.MET");
  TTreeReaderArray<Float_t> r_metETA(fReader, "MissingET.Eta");
  TTreeReaderArray<Float_t> r_metPHI(fReader, "MissingET.Phi");



  
  TTreeReaderArray<Float_t> r_weight(fReader, "Event.Weight");
  double totalweigt = 0;
  n_events = 0;
  int muonmas = 0;
  int muonmenos = 0;
  int electronmas = 0;
  int electronmenos = 0;

  // Event Loop
  boost::progress_display show_progress( fReader.GetEntries(0) );
  while (fReader.Next()) {
    ++show_progress;
    ld_PT = 0;
    sub_ld_PT = 0;
    numberbjet = 0;
    numberlepton = 0;
   
    totalweight = 3000*1000*r_weight[0];
    n_events++;
    //if (n_events %1000 == 0) std::cout << "Processing event number " << n_events << std::endl;

    numberlepton = r_muonpT.GetSize() + r_electronpT.GetSize();

    // Muon Loop
    for (unsigned long int imuon = 0; imuon < r_muonpT.GetSize(); ++imuon ) {
      //      std::cout << "Muon " << imuon << " with pT = " << r_muonpT[imuon] << std::endl;
      if  (r_muonCHARGE[imuon] == 1 ) {muonmas += 1;}
      else {muonmenos += 1;}
      if (ld_PT < r_muonpT[imuon])
        {
          sub_ld_PT = ld_PT;
          sub_ld_PHI = ld_PHI;
          sub_ld_ETA = ld_ETA;
          sub_ld_charge = ld_charge;
          sub_ld_type = ld_type;
          ld_PT = r_muonpT[imuon];
          ld_PHI = r_muonPHI[imuon];
          ld_ETA = r_muonETA[imuon];
          ld_charge = r_muonCHARGE[imuon];
          ld_type = 0;
        }
      else if (sub_ld_PT < r_muonpT[imuon])
        {
          sub_ld_PT = r_muonpT[imuon];
          sub_ld_PHI = r_muonPHI[imuon];
          sub_ld_ETA = r_muonETA[imuon];
          sub_ld_charge = r_muonCHARGE[imuon];
          sub_ld_type = 0;
        }
    }

    // electron Loop
    for (unsigned long int ielectron = 0; ielectron < r_electronpT.GetSize(); ++ielectron ) {
      //      std::cout << "Muon " << imuon << " with pT = " << r_muonpT[imuon] << std::endl;

      if  (r_electronCHARGE[ielectron] == 1 ){electronmas += 1;}
      else {electronmenos += 1;}

      if (ld_PT < r_electronpT[ielectron])
        {
          sub_ld_PT = ld_PT;
          sub_ld_PHI = ld_PHI;
          sub_ld_ETA = ld_ETA;
          sub_ld_charge = ld_charge;
          sub_ld_type = ld_type;
          ld_PT = r_electronpT[ielectron];
          ld_PHI = r_electronPHI[ielectron];
          ld_ETA = r_electronETA[ielectron];
          ld_charge = r_electronCHARGE[ielectron];
          ld_type = 1;
        }
      else if (sub_ld_PT < r_electronpT[ielectron])
        {
          sub_ld_PT = r_electronpT[ielectron];
          sub_ld_PHI = r_electronPHI[ielectron];
          sub_ld_ETA = r_electronETA[ielectron];
          sub_ld_charge = r_electronCHARGE[ielectron];
          sub_ld_type = 1;
        }
    }

    // jets loop
    for (unsigned long int ijet = 0; ijet < r_bjet.GetSize(); ++ijet ) {
      //printf("%d\n",r_bjet[ijet]);
      if (r_bjet[ijet] == 1)
        {
          numberbjet += 1;
        }
    }

    // MET
      met_PT = r_metmet[0];
      met_PHI = r_metPHI[0];
      met_ETA = r_metETA[0];
      met_miss = sqrt(2*ld_PT*sub_ld_PT*(1-abs(cos(ld_PHI-sub_ld_PHI)))); // transverse mass
      invarient_mass = sqrt(2*ld_PT*sub_ld_PT*(cosh(ld_ETA - sub_ld_ETA) - cos(ld_PHI - sub_ld_PHI) ));

      weight = r_weight[0];
    
    // fill events  cuts
      //corte 1 cut for minimun leptons sanity check
      if (numberlepton<2){continue;}
      if (ld_PT< 2){
        printf("Error a ld_pt with energy less then 2\n");
        continue;}
      if (sub_ld_PT< 2){
         printf("Error a sub_ld_pt with energy less then 2\n");
        continue;}

      // //corte1
        if (numberlepton != 3){continue;}

      // //corte2

      //   if (numberbjet != 0 ){continue;}


      // //corte3

      //   if (!((electronmenos >0 && electronmas >0) || (muonmenos>0 && muonmas>0))){continue;}

      // //corte4
      //  if (!(invarient_mass>81.2 && invarient_mass < 101.2 )){continue;}

      //  //corte5

      //  if (ld_PT<25 || ld_PT>80){continue;}

      //  //corte6

      //  if (sub_ld_PT<20 || sub_ld_PT>60){continue;}

      //  //corte7

      //  if (met_miss>110){continue;}

      //  //corte 8 

      //  if (met_PT>60){continue;}




 totalweigt +=3000*1000*r_weight[0];

    tree_out->Fill();
    
  }
  std::cout << "Number of events with weigths is " << totalweigt <<std::endl;   
  std::cout << "--- End of event loop " <<std::endl;   
}
