// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"

// Boost include(s):
#include <boost/program_options.hpp>
#include <boost/progress.hpp>

// TMVA include(s):
#include <TMVA/Reader.h>
#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAGui.h"

class diHiggsTMVAClassification {

public:
  
  diHiggsTMVAClassification();
  ~diHiggsTMVAClassification();
  void addMethod();
  //  void initialize();
  void execute();

private:

  Option option_;
  int n_events_;
  bool nathan_analysis;

  TFile *input_sgn;
  TFile *input_bkg_ZZ;
  TFile *input_bkg_WW;
  TFile *input_bkg_WZ;
  TFile *input_bkg_ttZ;
  TFile *input_bkg_ttW;


  TFile *outputFile;

  TTree *signalTree;
  TTree *bkgTree_ZZ;
  TTree *bkgTree_WW;
  TTree *bkgTree_WZ;
  TTree *bkgTree_ttZ;
  TTree *bkgTree_ttW;



// Tree files folder

  TString inputFolder = "/home/mhaacke/RichardoCode/Analysis/NeutralinoDM/build/";

  TString fname_sgn ;
  TString fname_bkg_ZZ ;
  TString fname_bkg_WW ;
  TString fname_bkg_WZ ;
  TString fname_bkg_ttZ ;
  TString fname_bkg_ttW;



 // TMVA input variables
  Float_t met;
  Float_t weight;
  
  Float_t ld_PT;
  Float_t ld_ETA;
  Float_t ld_PHI;
  Float_t ld_charge;
  Float_t ld_type;  
  
  Float_t sub_ld_PT;
  Float_t sub_ld_ETA;
  Float_t sub_ld_PHI;
  Float_t sub_ld_charge;
  Float_t sub_ld_type;

  Float_t numberbjet;
  Float_t numberlepton;


  Float_t met_PT;
  Float_t met_ETA;
  Float_t met_PHI;
  Float_t met_miss;

  Float_t invarient_mass;

  Float_t totalweight;
  

};

#endif
