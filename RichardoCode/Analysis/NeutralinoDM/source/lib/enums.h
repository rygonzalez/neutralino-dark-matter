// Model Options
enum Option {
  sgn = 1,
  ZZ = 2,
  WW = 3,
  WZ = 4,
  ttZ = 5,
  ttW = 6,
};
