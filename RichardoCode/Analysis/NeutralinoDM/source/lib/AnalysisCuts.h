// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"

// Boost include(s):
#include <boost/program_options.hpp>
#include <boost/progress.hpp>

// Text include(s):
#include <fstream>

// Vector include(s):
#include <vector>

//#include "TLorentzVector.h"
#include "TTreeReader.h"
#include "TTreeReaderArray.h"
#include <TMVA/Reader.h>

class AnalysisCuts {

public:
  
  AnalysisCuts(Option option);
  ~AnalysisCuts();
  void addBranches();
  void initialize();
  void finalize();
  void execute();

private:

  Option option_;
  TFile *output;
  TTree *tree_out;
  TFile *input;
  TTree *tree_in;
 
  // File name
  TString fname;
  TString outname;
  
  // Tree input variables
  Float_t met;
  Float_t weight;
  
  Float_t ld_PT;
  Float_t ld_ETA;
  Float_t ld_PHI;
  int ld_charge;
  int ld_type;  
  
  Float_t sub_ld_PT;
  Float_t sub_ld_ETA;
  Float_t sub_ld_PHI;
  int sub_ld_charge;
  int sub_ld_type;

  int numberbjet;
  int numberlepton;


  Float_t met_PT;
  Float_t met_ETA;
  Float_t met_PHI;
  Float_t met_miss;

  Float_t invarient_mass;

  Float_t totalweight;


 //output variable

  Float_t Met;
  Float_t Weight;
  
  Float_t Ld_PT;
  Float_t Ld_ETA;
  Float_t Ld_PHI;
  Float_t Ld_charge;
  Float_t Ld_type;  
  
  Float_t Sub_ld_PT;
  Float_t Sub_ld_ETA;
  Float_t Sub_ld_PHI;
  Float_t Sub_ld_charge;
  Float_t Sub_ld_type;

  Float_t Numberbjet;
  Float_t Numberlepton;

   Float_t Met_PT;
  Float_t Met_ETA;
  Float_t Met_PHI;
  Float_t Met_miss;

  Float_t Invarient_mass;

  Float_t Totalweight;

Float_t BDT_response;
  TMVA::Reader *reader;

  // Number of events
  int n_events;
  
  // Tree files folder
  TString filesFolder = "/home/rygonzalez/Public/100k_Events/";

  TTreeReader fReader;
};

#endif
