// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "AnalysisCuts.h"

AnalysisCuts::AnalysisCuts(Option option)
{
  
  std::cout << std::endl;
  std::cout << "--- AnalysisInput :: Analysis on " << option;

  option_ = option;

  if ( option_ == sgn )  std::cout << ": Signal" <<std::endl;
  if ( option_ == ZZ )  std::cout << ": Background ZZ" <<std::endl;
  if ( option_ == WW )  std::cout << ": Background WW" <<std::endl;
  if ( option_ == WZ )  std::cout << ": Background WZ" <<std::endl;
  if ( option_ == ttZ )  std::cout << ": Background ttZ" <<std::endl;
  if ( option_ == ttW )  std::cout << ": Background ttW" <<std::endl;

}


AnalysisCuts::~AnalysisCuts()
{
  //delete tree_in;
  //delete tree_out;
  delete input;
  delete output;
  

}
 
void AnalysisCuts::initialize()
{

  // Tree integer variables (no vectors) 
  Ld_PT = 0;
  Ld_ETA = 0;
  Ld_PHI = 0;
  Ld_charge = 0;
  Ld_type = 0;
  
  Sub_ld_PT = 0;
  Sub_ld_ETA = 0;
  Sub_ld_PHI = 0;
  Sub_ld_charge = 0;
  Sub_ld_type = 0;

  Numberbjet = 0;
  Numberlepton = 0;
  Totalweight = 0;

  Met_PT = 0;
  Met_ETA = 0;
  Met_PHI = 0;

    // Tree integer variables input 
  ld_PT = 0;
  ld_ETA = 0;
  ld_PHI = 0;
  ld_charge = 0;
  ld_type = 0;
  
  sub_ld_PT = 0;
  sub_ld_ETA = 0;
  sub_ld_PHI = 0;
  sub_ld_charge = 0;
  sub_ld_type = 0;

  numberbjet = 0;
  numberlepton = 0;
  totalweight = 0;

  met_PT = 0;
  met_ETA = 0;
  met_PHI = 0;
  BDT_response = 0;
}

void AnalysisCuts::addBranches()
{

  if ( option_ == sgn )  outname = "Signal_Ntuple_BDT.root";
  if ( option_ == ZZ )  outname = "ZZ_Ntuple_BDT.root";
  if ( option_ == WW )  outname = "WW_Ntuple_BDT.root";
  if ( option_ == WZ )  outname = "WZ_Ntuple_BDT.root";
  if ( option_ == ttZ )  outname = "ttZ_Ntuple_BDT.root";
  if ( option_ == ttW )  outname = "ttW_Ntuple_BDT.root";
  
  output = new TFile( outname , "RECREATE");
  tree_out = new TTree("neutralino", "neutralino");

  tree_out->Branch("weight", &Weight);
  tree_out->Branch("ld_PT", &Ld_PT);
  tree_out->Branch("ld_ETA", &Ld_ETA);
  tree_out->Branch("ld_PHI", &Ld_PHI);
  tree_out->Branch("ld_charge", &Ld_charge);
  tree_out->Branch("ld_type", &Ld_type);  

  tree_out->Branch("sub_ld_PT", &Sub_ld_PT);
  tree_out->Branch("sub_ld_ETA", &Sub_ld_ETA);
  tree_out->Branch("sub_ld_PHI", &Sub_ld_PHI);
  tree_out->Branch("sub_ld_charge", &Sub_ld_charge);
  tree_out->Branch("sub_ld_type", &Sub_ld_type);  

  tree_out->Branch("numberbjet", &Numberbjet);
  tree_out->Branch("numberlepton", &Numberlepton);

  tree_out->Branch("met_PT", &Met_PT);
  tree_out->Branch("met_ETA", &Met_ETA);
  tree_out->Branch("met_PHI", &Met_PHI);
  tree_out->Branch("met_miss", &Met_miss);

  tree_out->Branch("BDT_response",&BDT_response);

  tree_out->Branch("invarient_mass", &Invarient_mass);

  tree_out->Branch("totalweight", &Totalweight);

  


  
  if ( option_ == sgn )  fname = "Signal_Ntuple.root";
  if ( option_ == ZZ )  fname = "ZZ_Ntuple.root";
  if ( option_ == WW )  fname = "WW_Ntuple.root";
  if ( option_ == WZ )  fname = "WZ_Ntuple.root";
  if ( option_ == ttZ )  fname = "ttZ_Ntuple.root";
  if ( option_ == ttW )  fname = "ttW_Ntuple.root";

  
  if (!gSystem->AccessPathName( fname )) {
    input = TFile::Open( fname ); // check if file in local directory exists
  } 
  
  if (!input) {
    std::cout << "ERROR: could not open data file" << std::endl;
    exit(1);
  }
  
  std::cout << "--- Using input file: " << input->GetName() << std::endl;
  if ( option_ == sgn )  std::cout << "--- Select Signal sample" << std::endl;
  if ( option_ == ZZ )  std::cout << "--- Select Background ZZ sample" << std::endl;
  if ( option_ == WW )  std::cout << "--- Select Background WW sample" << std::endl;
  if ( option_ == WZ )  std::cout << "--- Select Background WZ sample" << std::endl;
  if ( option_ == ttZ )  std::cout << "--- Select Background ttZ sample" << std::endl;
  if ( option_ == ttW )  std::cout << "--- Select Background ttW sample" << std::endl;

  // Load TTreeReader with input tree file
  tree_in = (TTree*)input->Get("neutralino");
  tree_in->SetBranchAddress("weight", &weight);
  tree_in->SetBranchAddress("ld_PT", &ld_PT);
  tree_in->SetBranchAddress("ld_ETA", &ld_ETA);
  tree_in->SetBranchAddress("ld_PHI", &ld_PHI);
  tree_in->SetBranchAddress("ld_charge", &ld_charge);
  tree_in->SetBranchAddress("ld_type", &ld_type);  

  tree_in->SetBranchAddress("sub_ld_PT", &sub_ld_PT);
  tree_in->SetBranchAddress("sub_ld_ETA", &sub_ld_ETA);
  tree_in->SetBranchAddress("sub_ld_PHI", &sub_ld_PHI);
  tree_in->SetBranchAddress("sub_ld_charge", &sub_ld_charge);
  tree_in->SetBranchAddress("sub_ld_type", &sub_ld_type);  

  tree_in->SetBranchAddress("numberbjet", &numberbjet);
  tree_in->SetBranchAddress("numberlepton", &numberlepton);

  tree_in->SetBranchAddress("met_PT", &met_PT);
  tree_in->SetBranchAddress("met_ETA", &met_ETA);
  tree_in->SetBranchAddress("met_PHI", &met_PHI);
  tree_in->SetBranchAddress("met_miss", &met_miss);

  tree_in->SetBranchAddress("invarient_mass", &invarient_mass);

  tree_in->SetBranchAddress("totalweight", &totalweight);


  reader = new TMVA::Reader("!V:Silent=True",false);

  reader->AddVariable("ld_PT",&Ld_PT);
  reader->AddVariable("ld_ETA",&Ld_ETA);
  reader->AddVariable("ld_PHI",&Ld_PHI);
  reader->AddVariable("ld_charge",&Ld_charge);
  reader->AddVariable("ld_type",&Ld_type);
  reader->AddVariable("sub_ld_PT",&Sub_ld_PT);
  reader->AddVariable("sub_ld_ETA",&Sub_ld_ETA);
  reader->AddVariable("sub_ld_PHI",&Sub_ld_PHI);
  reader->AddVariable("sub_ld_charge",&Sub_ld_charge);
  reader->AddVariable("sub_ld_type",&Sub_ld_type);
  reader->AddSpectator("numberbjet",&Numberbjet);
  reader->AddSpectator("numberlepton",&Numberlepton);
  reader->AddVariable("met_PT",&Met_PT);
  reader->AddVariable("met_ETA",&Met_ETA);
  reader->AddVariable("met_PHI",&Met_PHI);
  reader->AddVariable("met_miss",&Met_miss);
  reader->AddVariable("invarient_mass",&Invarient_mass);
  reader->AddSpectator("totalweight",&Totalweight);
  reader->AddSpectator("weight",&Weight);

   reader->BookMVA("BDT method", "dataset/weights/TMVAClassification_BDT.weights.xml");

}

void AnalysisCuts::finalize()
{
  
  // Write and close output file
  output->Write();
  output->Close();
  
  // finalize
  std::cout << "--- Finalize. " <<std::endl;
  
}

void AnalysisCuts::execute()
{
  double totalweigt = 0;
  n_events = tree_in->GetEntries();
  // Event Loop
  boost::progress_display show_progress( n_events );

    for (Long64_t ievt=0; ievt < n_events ;ievt++) 
  {
    ++show_progress;
    tree_in->GetEntry(ievt);

  Weight = weight;
  
  Ld_PT = ld_PT;
  Ld_ETA =  ld_ETA ;
  Ld_PHI =  ld_PHI ;
  Ld_charge = (float)ld_charge ;
  Ld_type = (float)ld_type ;  
  
  Sub_ld_PT = sub_ld_PT;
  Sub_ld_ETA = sub_ld_ETA;
  Sub_ld_PHI = sub_ld_PHI;
  Sub_ld_charge = (float)sub_ld_charge;
  Sub_ld_type = (float)sub_ld_type;

  Numberbjet = (float)numberbjet;
  Numberlepton = (float)numberlepton;

  Met_PT = met_PT;
  Met_ETA = met_ETA;
  Met_PHI = met_PHI;
  Met_miss = met_miss;

  Invarient_mass = invarient_mass;

  Totalweight = totalweight;

  BDT_response = reader->EvaluateMVA("BDT method");
      //  //corte BDT

        if (BDT_response<0.5514){continue;}




    totalweigt +=totalweight;

    tree_out->Fill();
    
  }
  std::cout << "Number of events with weigths is " << totalweigt <<std::endl;   
  std::cout << "--- End of event loop " <<std::endl;   
}
