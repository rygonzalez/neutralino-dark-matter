// Local include(s):
#include "AnalysisBDT.h"
#include <iostream>
#include <iomanip>


// System include(s):
#include <unistd.h>

// Local include(s):
#include "AnalysisBDT.h"


diHiggsTMVAClassification::diHiggsTMVAClassification()
{
  std::cout << std::endl;
  std::cout << "[diHiggsTMVAClassification :: diHiggsTMVAClassification] ";
  
}

diHiggsTMVAClassification::~diHiggsTMVAClassification()
{
  
  // delete input_sgn;
  // delete input_bkg_ZZ;
  // //delete input_bkg_WW;
  // delete input_bkg_WZ;
  // delete input_bkg_ttZ;
  // delete input_bkg_ttW;
  
}

void diHiggsTMVAClassification::addMethod(){

  TString fname_sgn ;
  TString fname_bkg_ZZ ;
  TString fname_bkg_WW ;
  TString fname_bkg_WZ ;
  TString fname_bkg_ttZ ;
  TString fname_bkg_ttW ;

  // Read training and test data
  
    fname_sgn = inputFolder + "Signal_Ntuple.root";
    fname_bkg_ZZ = inputFolder + "ZZ_Ntuple.root";
    fname_bkg_WW = inputFolder + "WW_Ntuple.root";
    fname_bkg_WZ = inputFolder + "WZ_Ntuple.root";
    fname_bkg_ttZ = inputFolder + "ttZ_Ntuple.root";
    fname_bkg_ttW = inputFolder + "ttW_Ntuple.root";


  input_sgn = TFile::Open(fname_sgn);
  input_bkg_ZZ = TFile::Open(fname_bkg_ZZ);
  input_bkg_WW = TFile::Open(fname_bkg_WW);
  input_bkg_WZ = TFile::Open(fname_bkg_WZ);
  input_bkg_ttZ = TFile::Open(fname_bkg_ttZ);
  input_bkg_ttW = TFile::Open(fname_bkg_ttW);
 

  std::cout << "--- TMVAClassification       : Using nathan input signal file  " << input_sgn->GetName() << std::endl;
  std::cout << "--- TMVAClassification       : Using nathan input background file ZZ " << input_bkg_ZZ->GetName() << std::endl;
  std::cout << "--- TMVAClassification       : Using nathan input background file WW " << input_bkg_WW->GetName() << std::endl;
  std::cout << "--- TMVAClassification       : Using nathan input background file WZ " << input_bkg_WZ->GetName() << std::endl;
  std::cout << "--- TMVAClassification       : Using nathan input background file ttZ " << input_bkg_ttZ->GetName() << std::endl;
  std::cout << "--- TMVAClassification       : Using nathan input background file ttW " << input_bkg_ttW->GetName() << std::endl;
  

  signalTree = (TTree*)input_sgn->Get("neutralino");
  bkgTree_ZZ    = (TTree*)input_bkg_ZZ->Get("neutralino");
  bkgTree_WW    = (TTree*)input_bkg_WW->Get("neutralino");
  bkgTree_WZ    = (TTree*)input_bkg_WZ->Get("neutralino");
  bkgTree_ttZ    = (TTree*)input_bkg_ttZ->Get("neutralino");
  bkgTree_ttW  = (TTree*)input_bkg_ttW->Get("neutralino");

  // Create a ROOT output file where TMVA will store ntuples, histograms, etc. 
  TString outfileName;

  outfileName = TString( "TMVA.root" );
  
  outputFile = TFile::Open( outfileName, "RECREATE" );

  // Create the factory object. Later you can choose the methods                            
  // whose performance you'd like to investigate. The factory is                            
  // the only TMVA object you have to interact with                                         
  //                                                                                        
  // The first argument is the base of the name of all the                                  
  // weightfiles in the directory weight/                                                   
  //                                                                                        
  // The second argument is the output file for the training results                        
  // All TMVA output can be suppressed by removing the "!" (not) in                         
  // front of the "Silent" argument in the option string 
  TMVA::Factory * factory = new TMVA::Factory( "TMVAClassification", outputFile,"!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G;D:AnalysisType=Classification" );
  TMVA::DataLoader *dataloader;

  dataloader=new TMVA::DataLoader("dataset");

  
  dataloader->AddVariable("ld_PT","ld_PT", "", 'D' );
  dataloader->AddVariable("ld_ETA","ld_ETA", "", 'D' );
  dataloader->AddVariable("ld_PHI","ld_PHI", "", 'D' );
  dataloader->AddVariable("ld_charge","ld_charge", "", 'I' );
  dataloader->AddVariable("ld_type","ld_type", "", 'I' );
  dataloader->AddVariable("sub_ld_PT","sub_ld_PT", "", 'D' );
  dataloader->AddVariable("sub_ld_ETA","sub_ld_ETA", "", 'D' );
  dataloader->AddVariable("sub_ld_PHI","sub_ld_PHI", "", 'D' );
  dataloader->AddVariable("sub_ld_charge","sub_ld_charge", "", 'I' );
  dataloader->AddVariable("sub_ld_type","sub_ld_type", "", 'I' );
  dataloader->AddSpectator("numberbjet","numberbjet", "", 'I' );
  dataloader->AddSpectator("numberlepton","numberlepton", "", 'I' );
  dataloader->AddVariable("met_PT","met_PT", "", 'D' );
  dataloader->AddVariable("met_ETA","met_ETA", "", 'D' );
  dataloader->AddVariable("met_PHI","met_PHI", "", 'D' );
  dataloader->AddVariable("met_miss","met_miss", "", 'D' );
  dataloader->AddVariable("invarient_mass","invarient_mass", "", 'D' );

  dataloader->AddSpectator("totalweight","totalweight", "", 'D' );
  dataloader->AddSpectator("weight","weight", "", 'D' );

  // global event weights per tree (see below for setting event-wise weights) - probably global variables instead of local * dan



  // You can add an arbitrary number of signal or background trees
  dataloader->AddSignalTree    (signalTree , 1.0);

  dataloader->AddBackgroundTree(bkgTree_ZZ  ,  1.0);
  dataloader->AddBackgroundTree(bkgTree_WW  ,  1.0);
  dataloader->AddBackgroundTree(bkgTree_WZ  ,  1.0);
  dataloader->AddBackgroundTree(bkgTree_ttZ  ,  1.0);
  dataloader->AddBackgroundTree(bkgTree_ttW  ,  1.0);

  // Set individual event weights (the variables must exist in the original TTree) we have to fix this *dan
   
  dataloader->SetSignalWeightExpression( "totalweight" );
  dataloader->SetBackgroundWeightExpression( "totalweight " );

  // Tell the dataloader how to use the training and testing events
  // If no numbers of events are given, half of the events in the tree are used
  // for training, and the other half for testing:
  //
  // For all events in the samples tree file
  TCut mycut = "";
  //-----------------------------------------------------------------------
  //           DEFINITIVE

  dataloader->PrepareTrainingAndTestTree( mycut, "SplitMode=random:V:VerboseLevel=Verbose" );
      
 
  factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT","!H:!V:NTrees=1000:MinNodeSize=3%:MaxDepth=4:BoostType=RealAdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=50:DoBoostMonitor=true");

  
  // Now you can tell the factory to train, test, and evaluate the MVAs                     
  //                                                                                        
  // Train MVAs using the set of training events
  factory->TrainAllMethods();
  
  // Evaluate all MVAs using the set of test events
  factory->TestAllMethods();
  
  // Evaluate and compare performance of all configured MVAs
  factory->EvaluateAllMethods();

  // Save the output
  outputFile->Close();
  
  std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
  std::cout << "==> TMVAClassification is done!" << std::endl;
  
  delete factory;
  delete dataloader;
  
}

void diHiggsTMVAClassification::execute()
{

  // This loads the library
  TMVA::Tools::Instance();
  std::cout << "[diHiggsTMVAClassification :: Finalize]" <<std::endl;
    
}
