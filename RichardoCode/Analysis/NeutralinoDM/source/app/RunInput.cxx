// System include(s):
#include <iostream>

// Local include(s):
#include "AnalysisInput.h"

// ROOT include(s):
#include "TTree.h"

int Run(Option opt) {

   // start timing

  // Options: sgn, ZZ, WW, WZ, ttZ, ttW 
  AnalysisInput *NeutralinoDM = new AnalysisInput(opt);
  NeutralinoDM->initialize();
  NeutralinoDM->addBranches();
  NeutralinoDM->execute();
  NeutralinoDM->finalize();
  delete NeutralinoDM;
  
  // Return gracefully:
  return 0;
}

int main(int argc, char const *argv[])
{
	 boost::progress_timer t;
	
	 Run(sgn);
	 Run(ZZ);
	 Run(WW);
	 Run(WZ);
	 Run(ttZ);
	 Run(ttW);

	return 0;
}

