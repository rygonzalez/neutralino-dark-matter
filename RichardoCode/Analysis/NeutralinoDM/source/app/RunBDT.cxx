// System include(s):
#include <iostream>

// Local include(s):
#include "AnalysisBDT.h"

// ROOT include(s):
#include "TTree.h"

int Run() {

   // start timing

  // Options: sgn, ZZ, WW, WZ, ttZ, ttW 
  diHiggsTMVAClassification *NeutralinoDM = new diHiggsTMVAClassification();
  NeutralinoDM->addMethod();
  NeutralinoDM->execute();
  
  // Return gracefully:
  return 0;
}

int main(int argc, char const *argv[])
{
	 boost::progress_timer t;
	
	 Run();

	return 0;
}

