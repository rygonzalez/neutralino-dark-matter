#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "NeutralinoDM" for configuration ""
set_property(TARGET NeutralinoDM APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(NeutralinoDM PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libNeutralinoDM.so"
  IMPORTED_SONAME_NOCONFIG "libNeutralinoDM.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS NeutralinoDM )
list(APPEND _IMPORT_CHECK_FILES_FOR_NeutralinoDM "${_IMPORT_PREFIX}/lib/libNeutralinoDM.so" )

# Import target "RunInput" for configuration ""
set_property(TARGET RunInput APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(RunInput PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/bin/RunInput"
  )

list(APPEND _IMPORT_CHECK_TARGETS RunInput )
list(APPEND _IMPORT_CHECK_FILES_FOR_RunInput "${_IMPORT_PREFIX}/bin/RunInput" )

# Import target "RunCut" for configuration ""
set_property(TARGET RunCut APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(RunCut PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/bin/RunCut"
  )

list(APPEND _IMPORT_CHECK_TARGETS RunCut )
list(APPEND _IMPORT_CHECK_FILES_FOR_RunCut "${_IMPORT_PREFIX}/bin/RunCut" )

# Import target "RunBDT" for configuration ""
set_property(TARGET RunBDT APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(RunBDT PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/bin/RunBDT"
  )

list(APPEND _IMPORT_CHECK_TARGETS RunBDT )
list(APPEND _IMPORT_CHECK_FILES_FOR_RunBDT "${_IMPORT_PREFIX}/bin/RunBDT" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
