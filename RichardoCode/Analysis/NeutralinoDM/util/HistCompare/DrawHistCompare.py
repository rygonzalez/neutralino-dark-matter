import ROOT 
ROOT.gROOT.SetBatch(True)

import os
cwd = os.getcwd()
stylepath = cwd + '/AtlasStyle/'
print(stylepath)

ROOT.gROOT.LoadMacro( stylepath + "AtlasStyle.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasLabels.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import *

#Flags
doLogX = False
doLogY = False
Normalise = True
BDT = True

basepath = '/home/rygonzalez/WorkArea/DataAnalysis/RichardoCode/Analysis/NeutralinoDM/tuples/'
tree = 'neutralino'
hists = ['invarient_mass','met_miss','ld_PT','ld_ETA','ld_PHI','sub_ld_PT','sub_ld_ETA','sub_ld_PHI','numberbjet','numberlepton','met_PT','met_ETA','met_PHI']
stacks = ['hs', 'hs_merge']
if BDT:
    samples = {
    'sgn': [basepath + 'Signal_Ntuple_BDT.root'],
    'ZZ': [basepath + 'ZZ_Ntuple_BDT.root'],
    'WW': [basepath + 'WW_Ntuple_BDT.root'],
    'WZ': [basepath + 'WZ_Ntuple_BDT.root'],
    'ttZ': [basepath + 'ttZ_Ntuple_BDT.root'],
    'ttW': [basepath + 'ttW_Ntuple_BDT.root'],
    }
else:
    samples = {
    'sgn': [basepath + 'Signal_Ntuple.root'],
    'ZZ': [basepath + 'ZZ_Ntuple.root'],
    'WW': [basepath + 'WW_Ntuple.root'],
    'WZ': [basepath + 'WZ_Ntuple.root'],
    'ttZ': [basepath + 'ttZ_Ntuple.root'],
    'ttW': [basepath + 'ttW_Ntuple.root'],
    }

outfile = ROOT.TFile(cwd + '/plotsCompare/hists.root','RECREATE')

# CREATING HISTOGRAMS
h = {}
for sample in samples.keys():
    h[sample] = {}

    for hist in hists:
        h[sample][hist] = {}

        n = 'h_%s_%s' % (sample, hist)

        if 'PT' in hist  :
            h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 700)
        elif 'miss' in hist or 'invarient_mass' in hist:
             h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 700)
        else:
            h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -5, 5)

        h[sample][hist].SetDirectory(outfile)

        
# FILLING
for hist in hists:
    print 'processing histogram: %s' % hist

    for sample in samples.keys():
        print 'processing sample: %s' % sample
        l = samples[sample]

        for fname in l:
            print '            > %s' % fname
            f = ROOT.TFile.Open(fname)
            t = f.Get(tree)

            if 'PT' in hist:
                n = 'htmp(50,0,700)'
            elif 'miss' in hist or 'invarient_mass' in hist:
                n = 'htmp(50,0,700)'
            else:
                n = 'htmp(50,-5,5)'

            if t.Draw('%s>>%s' % (hist, n),
                      '1*totalweight', #should be 1*weight 
                      'goff'
            ):
                h[sample][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)
                
del f

# NORMALIZE & CONFIG
for sample in samples:
    for hist in hists:

        if Normalise==True:
            # Histogram normalization
            norm = 1;
            if (h[sample][hist].Integral() == 0):scale = 0
            else: scale = norm/h[sample][hist].Integral()
            h[sample][hist].Scale(scale)

        # Line width
        if sample == 'sgn': h[sample][hist].SetLineWidth(1)
        else: h[sample][hist].SetLineWidth(5)
        # Line style
        h[sample][hist].SetLineStyle(1)
        
# PLOTTING
for hist in hists:

    # Style and colour
    h['sgn'][hist].SetLineColor(ROOT.kBlack)
    h['sgn'][hist].SetFillColor(ROOT.kRed)    
    h['ZZ'][hist].SetLineColor(ROOT.kGreen)
    h['WW'][hist].SetLineColor(ROOT.kCyan)
    h['WZ'][hist].SetLineColor(ROOT.kBlue)
    h['ttZ'][hist].SetLineColor(ROOT.kMagenta)
    h['ttW'][hist].SetLineColor(ROOT.kOrange)
#    h['bkg_yyjj'][hist].SetLineColor(ROOT.kCyan)
#    h['bkg_yccj'][hist].SetLineColor(ROOT.kAzure)
#    h['bkg_bbjj'][hist].SetLineColor(ROOT.kTeal)
#    h['bkg_yyZbb'][hist].SetLineColor(ROOT.kSpring)
#    h['bkg_bbH'][hist].SetLineColor(ROOT.kYellow)
#    h['bkg_ggH'][hist].SetLineColor(ROOT.kOrange)
#    h['bkg_ttH'][hist].SetLineColor(ROOT.kPink)
#    h['bkg_ZH'][hist].SetLineColor(ROOT.kBlack)


    # Tex in canvas
    tex = ROOT.TLatex(0.2, 0.8, "#sqrt{s} = 14 TeV   L_{int} = 3000 fb^{-1}")
    tex.SetName('tex')
    tex.SetNDC(True)
    tex.SetTextSize(0.04)

    #legend configuration
    xmin = 0.73
    xmax = 0.88 
    ymin = 0.65 # was 0.55
    ymax = 0.90 # was 0.90
        
    legend = ROOT.TLegend(xmin, ymin, xmax, ymax) 
    legend.SetFillColor(0)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(42)
    legend.SetTextSize(0.035) 
    legend.SetName('neutralino')
    legend.SetShadowColor(0)
    
    legend.AddEntry(h['sgn'][hist],'signal', 'f')
    legend.AddEntry(h['ZZ'][hist],'ZZ', 'l')
    legend.AddEntry(h['WW'][hist],'WW', 'l')
    legend.AddEntry(h['WZ'][hist],'WZ', 'l')
    legend.AddEntry(h['ttZ'][hist],'ttZ', 'l')
    legend.AddEntry(h['ttW'][hist],'ttW', 'l')
#    legend.AddEntry(h['bkg_yyjj'][hist],'jj#gamma#gamma', 'l')
#    legend.AddEntry(h['bkg_yccj'][hist],'jcc#gamma', 'l')
#    legend.AddEntry(h['bkg_bbjj'][hist],'bbjj', 'l')
#    legend.AddEntry(h['bkg_yyZbb'][hist],'bbZ#gamma#gamma', 'l')
#    legend.AddEntry(h['bkg_bbH'][hist],'bbH', 'l')
#    legend.AddEntry(h['bkg_ggH'][hist],'ggH', 'l')
#    legend.AddEntry(h['bkg_ttH'][hist],'ttH', 'l')
#    legend.AddEntry(h['bkg_ZH'][hist],'ZH', 'l')
    
    # THStack
    hs  = ROOT.THStack("hsStack","Histogram Comparison")

    hs.Add(h['sgn'][hist])
    hs.Add(h['ZZ'][hist])
    hs.Add(h['WW'][hist])
    hs.Add(h['WZ'][hist])
    hs.Add(h['ttZ'][hist])
    hs.Add(h['ttW'][hist])
#    hs.Add(h['bkg_yyjj'][hist])
#    hs.Add(h['bkg_yccj'][hist])
#    hs.Add(h['bkg_bbjj'][hist])
#    hs.Add(h['bkg_yyZbb'][hist])
#    hs.Add(h['bkg_bbH'][hist])
#    hs.Add(h['bkg_ggH'][hist])
#    hs.Add(h['bkg_ttH'][hist])
#    hs.Add(h['bkg_ZH'][hist])
        
    c = ROOT.TCanvas('c', 'c', 800, 800)
    if doLogY == True: c.SetLogy()
    if doLogX == True: c.SetLogx()    
    hs.Draw('nostack, hist')

    # Y-axis title
    if ( Normalise == True ):
        #hs.GetYaxis().SetTitle('Normalised Events')
        # Signal Normalisation bins
        hs.GetYaxis().SetTitle('Normalised events / %.2f' % (h['sgn'][hist].GetBinWidth(1)))
        hs.GetYaxis().SetLabelSize(0.04)
    else:
        hs.GetYaxis().SetTitle('Events')
        hs.GetYaxis().SetLabelSize(0.04)
    # X-axis title
    if 'PT' in hist or 'miss' in hist or 'invarient_mass' in hist:
        hs.GetXaxis().SetTitle('MET [GeV]')
    else:
        hs.GetXaxis().SetTitle('default')
    # X-axis range
    if 'PT' in hist or 'miss' in hist or 'invarient_mass' in hist:
        hs.GetXaxis().SetRangeUser(0,500)
        hs.GetXaxis().SetLabelSize(0.04)
    else:
        hs.GetXaxis().SetRangeUser(-5,5)
        hs.GetXaxis().SetLabelSize(0.04)

    if Normalise == True: hs.SetMaximum(hs.GetMaximum() * 0.48) #was 0.78, 0.68, 0.58
    else: hs.SetMaximum(hs.GetMaximum() * 80) #was 60
#    else: hs.SetMaximum(pow(10,6)) #was 0.9

    
    legend.Draw()
    tex.Draw()
    ATLASLabel(0.2,0.87,"Work in progress")
    c.Update()
    if BDT:
        if ( Normalise == True ):
            c.SaveAs( cwd + '/plotsCompare/Normalise/%s_BDT.png' % (hist))
        else:
            c.SaveAs( cwd + '/plotsCompare/Default/%s_BDT.png' % (hist))
    else:
        if ( Normalise == True ):
            c.SaveAs( cwd + '/plotsCompare/Normalise/%s.png' % (hist))
        else:
            c.SaveAs( cwd + '/plotsCompare/Default/%s.png' % (hist))


